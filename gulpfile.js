let gulp = require('gulp');
let sass = require('gulp-sass');
let browserSync = require('browser-sync');
let concat = require('gulp-concat');
let terser = require('gulp-terser');

gulp.task('sass', function(){
    return gulp.src('app/sass/**/*.scss')
        .pipe(sass())
        .pipe(gulp.dest('app/css'))
        .pipe(browserSync.stream());
});
gulp.task('browser-sync', function() {
    browserSync({
        server: {
            baseDir: 'app'
        },
        notify: false
    });
});
gulp.task('scripts', function () {
    return gulp.src(
        [
            'app/libs/jquery.js',
            'app/libs/slick.js',
            'app/libs/jquery-mask.js',
            'app/js/func.js',
        ]
    )
        .pipe(concat('libs.min.js'))
        .pipe(terser())
        .pipe(gulp.dest('app/js'))
});

gulp.task('watch', ['browser-sync', 'scripts', 'sass'], function (){
    gulp.watch('app/sass/**/*.scss', ['sass']);
    gulp.watch('app/*.html', browserSync.reload); 
    gulp.watch('app/js/**/*.js', browserSync.reload); 
});

gulp.task('default', ['watch']);